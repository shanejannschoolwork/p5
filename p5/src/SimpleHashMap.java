///////////////////////////////////////////////////////////////////////////////
// Main Class File:  MapBenchmark.java
// File:             SimpleHashMap.java
// Semester:         CS367 Fall 2014
//
// Author:           Shane Jann jann@wisc.edu
// CS Login:         shane
// Lecturer's Name:  Jim Skrentny
// Lab Section:      Lec 002
//////////////////////////// 80 columns wide //////////////////////////////////


import java.util.Iterator;
import java.util.LinkedList;
/**
 *
 * A map is a data structure that creates a key-value mapping. Keys are
 * unique in the map. That is, there cannot be more than one value associated
 * with a same key. However, two keys can map to a same value.
 *
 * The SimpleHashMap takes two generic parameters, K
 * and V, standing for the types of keys and values respectively.
 *
 */
public class SimpleHashMap<K extends Comparable<K>,V> implements SimpleMapADT<K , V> {


	private int[] tableSizes = { 11, 23, 47, 97, 197, 397, 797, 1597, 3203, 6421, 12853, 25717, 51437, 102877,
			205759, 411527, 823117, 1646237, 3292489, 6584983, 13169977, 26339969, 52679969, 105359939, 210719881, 
			421439783, 842879579, 1685759167};
	private double lf = 0.75;

	private LinkedList<Entry<K,V>>[] table;
	private int currTSIndex = 0;
	private int numItems = 0;

	@SuppressWarnings("unchecked")
	public SimpleHashMap(){
		table = new LinkedList[tableSizes[currTSIndex]];
	} 


	/**
	 * returns the hash index of a given Key k
	 * 
	 * @param k the key that is inputed into the hash function
	 * @return the hash index
	 */
	private int hash(K k) {
		int hashIndex = k.hashCode() % tableSizes[currTSIndex];

		if(hashIndex < 0){
			hashIndex += tableSizes[currTSIndex];
		}
		return hashIndex;
	} 


	/**
	 * Returns the value to which the specified key is mapped, or null if this
	 * map contains no mapping for the key.
	 *
	 * @param key the key whose associated value is to be returned
	 * @return the value to which the specified key is mapped, or null
	 * if this map contains no mapping for the key
	 * @throws NullPointerException if the specified key is null
	 */

	public V get(K key) {
		if(key == null){
			throw new NullPointerException();
		}
		Iterator<Entry<K, V>> itr = table[hash(key)].iterator();
		V result = null;

		while(itr.hasNext()){
			Entry<K, V> temp = itr.next();
			if(temp.getKey().equals(key)){
				result = temp.getValue();
			}
		}
		return result;

	}   



	/**
	 * Associates the specified value with the specified key in this map.
	 * Neither the key nor the value can be null. If the map
	 * previously contained a mapping for the key, the old value is replaced.
	 *
	 * @param key key with which the specified value is to be associated
	 * @param value value to be associated with the specified key
	 * @return the previous value associated with key, or
	 * null if there was no mapping for key.
	 * @throws NullPointerException if the key or value is null
	 */
	public V put(K key, V value) {

		if(key == null || value == null){
			throw new NullPointerException();
		}
		if(numItems / tableSizes[currTSIndex] > lf){
			resize();
		}

		//if statement occurs if there is no value at the hash index
		if(table[hash(key)] == null){
			table[hash(key)] = new LinkedList<Entry<K,V>>();
			table[hash(key)].add(new Entry<K, V>(key, value));
			numItems++;
		}

		//If there is a value...
		else{
			int i = 0;
			try{
				while(table[hash(key)].get(i) != null || 
						table[hash(key)].get(i).getKey() != key){


					//if key is found, replaces old value and returns old value
					if(table[hash(key)].get(i).getKey().equals(key)){
						V val = table[hash(key)].get(i).getValue();
						table[hash(key)].get(i).setValue(value);
						return val;
					}
					i++;
				}
			} catch(IndexOutOfBoundsException e){
				//Reaches end of the LinkedList
			}

			//if key is NOT found, simply adds the new value in and return null
			table[hash(key)].add(new Entry<K, V>(key, value));
			numItems++;

		}
		return null;
	}   




	/**
	 * Removes the mapping for the specified key from this map if present. This
	 * method does nothing if the key is not in the map.
	 *
	 * @param key key whose mapping is to be removed from the map
	 * @return the previous value associated with key, or null
	 * if there was no mapping for key.
	 * @throws NullPointerException if key is null
	 */

	public V remove(K key) {
		if(key == null){
			throw new NullPointerException();
		}
		Iterator<Entry<K, V>> itr = table[hash(key)].iterator();
		V result = null;

		while(itr.hasNext()){
			K tempK = itr.next().getKey();
			if(tempK.equals(key)){
				result = table[hash(key)].remove().getValue();
				return result;
			}
		}
		return result;
	}   

	/**
	 * Returns the greatest key less than or equal to the given key,
	 * or null if there is no such key. 
	 * Throws NullPointerException if key is null. 
	 * @param key key whose floor should be found
	 * @return the largest key smaller than the one passed to it
	 * @throws NullPointerException if key is null
	 */
	public K floorKey(K key){
		if(key == null){
			throw new NullPointerException();
		}    
		int hashIndex = 0;
		K flrKey = null;
		while(hashIndex < table.length){

			if(table[hashIndex] == null){
				//if null, move on to next index
			}
			else{
				Iterator<Entry<K,V>> itr = table[hashIndex].iterator();
				while(itr.hasNext()){

					K tempKey = itr.next().getKey();
					if(tempKey.compareTo(key) < 0){
						if(flrKey == null || tempKey.compareTo(flrKey) >= 0){
							flrKey = tempKey;
						}
					}
				}
			}
			hashIndex++;
		}
		return flrKey;
	}

	@SuppressWarnings("unchecked")
	/**
	 * This helper method resizes the hash table whenever the load factor
	 * is exceeded. It then copies over all values
	 */
	private void resize(){
		currTSIndex++;
		LinkedList<Entry<K,V>>[] tempArr = 
				new LinkedList[tableSizes[currTSIndex]];

		for(int i = 0; i < table.length; i++){
			if(table[i] != null){
				while(!table[i].isEmpty()){
					Entry<K, V> tempEnt = table[i].getFirst();
					int tempIndex = hash(tempEnt.getKey());
					if(tempArr[tempIndex] == null){
						tempArr[tempIndex] = new LinkedList<Entry<K, V>>();
					}

					tempArr[tempIndex]
							.add(table[i].remove(0));
				}
			}

		}
		table = tempArr;
	}
}
