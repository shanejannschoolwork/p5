///////////////////////////////////////////////////////////////////////////////
// Title:            MapBenchmark.java
// Files:            SimpleTreeMap.java, SimpleHashMap.java, Entry.java
// Semester:         CS367 Fall 2014
//
// Author:           Shane Jann
// Email:            jann@wisc.edu
// CS Login:         shane
// Lecturer's Name:  Jim Skrentny
// Lab Section:      Lec 002
//////////////////////////// 80 columns wide //////////////////////////////////
import java.util.*;
import java.io.File;
import java.io.FileNotFoundException;
public class MapBenchmark{

	/**
	 * Runs the program MapBenchmark which requires two command-line args.
	 * Displays the runtimes of the hashMap operations and treeMap operations
	 * 
	 * @param args two required command line arguments in the format: 
	 *  MapBenchmark fileName.txt numIterations 
	 */
	public static void main(String[] args) {
		if(args.length == 2){
			int numIter = 0; //number of iterations to run
			try{
				numIter = Integer.parseInt(args[1]);
			} catch(NumberFormatException e){

				System.out.println("Usage: MapBenchmark fileName.txt " +
						"numIterations");
				System.exit(0);
			} 

			File fileName = new File(args[0]);
			Scanner scnr = null;
			try {
				scnr = new Scanner(fileName);

			} catch (FileNotFoundException e) {
				System.out.println("File " + fileName + " Not Found");
				System.exit(0);
			}

			SimpleTreeMap<Integer, String> treeMap =
					new SimpleTreeMap<Integer, String>();
			SimpleHashMap<Integer, String> hashMap =
					new SimpleHashMap<Integer, String>();

			ArrayList<Entry<Integer, String>> entries = 
					new ArrayList<Entry<Integer, String>>();

			String[] oneLine;   // of the file

			//Collect Info from file
			//oneLine[0] will represent the key while 
			//oneLine[1] will represent the value
			while(scnr.hasNext()){
				oneLine = scnr.nextLine().split(" ");
				Entry<Integer, String> entry = 
						new Entry<Integer, String>(Integer.parseInt(
								oneLine[0]), oneLine[1]);
				entries.add(entry);
			}

			//ArrayLists that will store the data in the loop below
			ArrayList<Long> PopulateTreeTime = new ArrayList<Long>();
			ArrayList<Long> PopulateHashTime = new ArrayList<Long>();
			ArrayList<Long> GetTreeTime = new ArrayList<Long>();
			ArrayList<Long> GetHashTime = new ArrayList<Long>();
			ArrayList<Long> FlrKeyTreeTime = new ArrayList<Long>();
			ArrayList<Long> FlrKeyHashTime = new ArrayList<Long>();
			ArrayList<Long> RmvTreeTime = new ArrayList<Long>();
			ArrayList<Long> RmvHashTime = new ArrayList<Long>();

			for(int ndx = 0;ndx < numIter; ndx++){

				//Populate treeMap
				long startPopT = System.currentTimeMillis();
				for(int i = 0; i < entries.size(); i++){
					treeMap.put(entries.get(i).getKey(),
							entries.get(i).getValue());
				}
				long elapsedPopT = System.currentTimeMillis() - startPopT;
				PopulateTreeTime.add(elapsedPopT);

				//Populate hashMap
				long startPopH = System.currentTimeMillis();
				for(int i = 0; i < entries.size(); i++){
					hashMap.put(entries.get(i).getKey(),
							entries.get(i).getValue());
				}
				long elapsedPopH = System.currentTimeMillis() - startPopH;
				PopulateHashTime.add(elapsedPopH);

				//Get from treeMap
				long startGetT = System.currentTimeMillis();
				for(int i = 0; i < entries.size(); i++){
					treeMap.get(entries.get(i).getKey());
				}
				long elapsedGetT = System.currentTimeMillis() - startGetT;
				GetTreeTime.add(elapsedGetT);

				//Get from hashMap
				long startGetH = System.currentTimeMillis();
				for(int i = 0; i < entries.size(); i++){
					hashMap.get(entries.get(i).getKey());
				}
				long elapsedGetH = System.currentTimeMillis() - startGetH;
				GetHashTime.add(elapsedGetH);

				//Floor Key treeMap
				long startFlrT = System.currentTimeMillis();
				for(int i = 0; i < entries.size(); i++){
					treeMap.floorKey(entries.get(i).getKey());
				}
				long elapsedFlrT = System.currentTimeMillis() - startFlrT;
				FlrKeyTreeTime.add(elapsedFlrT);

				//Floor Key hashMap
				long startFlrH = System.currentTimeMillis();
				for(int i = 0; i < entries.size(); i++){
					hashMap.floorKey(entries.get(i).getKey());
				}
				long elapsedFlrH = System.currentTimeMillis() - startFlrH;
				FlrKeyHashTime.add(elapsedFlrH);

				//Remove from  treeMap
				long startRmvT = System.currentTimeMillis();
				for(int i = 0; i < entries.size(); i++){
					treeMap.remove(entries.get(i).getKey());
				}
				long elapsedRmvT = System.currentTimeMillis() - startRmvT;
				RmvTreeTime.add(elapsedRmvT);

				//Remove from hashMap
				long startRmvH = System.currentTimeMillis();
				for(int i = 0; i < entries.size(); i++){
					hashMap.remove(entries.get(i).getKey());
				}
				long elapsedRmvH = System.currentTimeMillis() - startRmvH;
				RmvHashTime.add(elapsedRmvH);
				

			}

			//POPULATE TREE OUTPUT
			long totalTreePop = 0;
			long minTPop = 100000; //placeholder
			long maxTPop = 0;      //placeholder
			for(int i = 0; i < PopulateTreeTime.size(); i++){
				totalTreePop += PopulateTreeTime.get(i);
				if(PopulateTreeTime.get(i) < minTPop){
					minTPop = PopulateTreeTime.get(i);
				}
				if(PopulateTreeTime.get(i) > maxTPop){
					maxTPop = PopulateTreeTime.get(i);
				}
			}
			float tPopMean = totalTreePop / (float)numIter;
			float tPopStdDev = (float) Math.pow(calcVari(
					PopulateTreeTime, tPopMean), 0.5);


			//POPULATE HASH OUTPUT
			long totalHashPop = 0;
			long minHPop = 100000; //placeholder
			long maxHPop = 0;      //placeholder
			for(int i = 0; i < PopulateHashTime.size(); i++){
				totalHashPop += PopulateHashTime.get(i);
				if(PopulateHashTime.get(i) < minHPop){
					minHPop = PopulateHashTime.get(i);
				}
				if(PopulateHashTime.get(i) > maxHPop){
					maxHPop = PopulateHashTime.get(i);
				}
			}
			float hPopMean = totalHashPop / (float)numIter;
			float hPopStdDev = (float) Math.pow(calcVari(
					PopulateHashTime, hPopMean), 0.5);


			//GET TREE OUTPUT
			long totalTreeGet = 0;
			long minTGet = 100000; //placeholder
			long maxTGet = 0;      //placeholder
			for(int i = 0; i < GetTreeTime.size(); i++){
				totalTreeGet += GetTreeTime.get(i);
				if(GetTreeTime.get(i) < minTGet){
					minTGet = GetTreeTime.get(i);
				}
				if(GetTreeTime.get(i) > maxTGet){
					maxTGet = GetTreeTime.get(i);
				}
			}
			float tGetMean = totalTreeGet / (float)numIter;
			float tGetStdDev = (float) Math.pow(calcVari(
					GetTreeTime, tGetMean), 0.5);

			//GET HASH OUTPUT
			long totalHashGet = 0;
			long minHGet = 100000; //placeholder
			long maxHGet = 0;      //placeholder
			for(int i = 0; i < GetHashTime.size(); i ++){
				totalHashGet += GetHashTime.get(i);
				if(GetHashTime.get(i) < minHGet){
					minHGet = GetHashTime.get(i);
				}
				if(GetHashTime.get(i) > maxHGet){
					maxHGet = GetHashTime.get(i);
				}
			}
			float hGetMean = totalHashGet / (float)numIter;
			float hGetStdDev = (float) Math.pow(calcVari(
					GetHashTime, hGetMean), 0.5);

			//TREE FLOORKEY OUTPUT
			long totalTreeFloorKey = 0;
			long minTFlr = 100000; //placeholder
			long maxTFlr = 0;      //placeholder
			for(int i = 0; i < FlrKeyTreeTime.size(); i ++){
				totalTreeFloorKey += FlrKeyTreeTime.get(i);
				if(FlrKeyTreeTime.get(i) < minTFlr){
					minTFlr = FlrKeyTreeTime.get(i);
				}
				if(FlrKeyTreeTime.get(i) > maxTFlr){
					maxTFlr = FlrKeyTreeTime.get(i);
				}
			}
			float tFlrMean = totalTreeFloorKey / (float)numIter;
			float tFlrStdDev = (float) Math.pow(calcVari(
					FlrKeyTreeTime, tFlrMean), 0.5);

			//HASH FLOORKEY OUTPUT
			long totalHashFloorKey = 0;
			long minHFlr = 100000; //placeholder
			long maxHFlr = 0;      //placeholder
			for(int i = 0; i < FlrKeyHashTime.size(); i ++){
				totalHashFloorKey += FlrKeyHashTime.get(i);
				if(FlrKeyHashTime.get(i) < minHFlr){
					minHFlr = FlrKeyHashTime.get(i);
				}
				if(FlrKeyHashTime.get(i) > maxHFlr){
					maxHFlr = FlrKeyHashTime.get(i);
				}
			}
			float hFlrMean = totalHashFloorKey / (float)numIter;
			float hFlrStdDev = (float) Math.pow(calcVari(
					FlrKeyHashTime, hFlrMean), 0.5);

			//TREE REMOVE OUTPUT
			long totalTreeRemove = 0;
			long minTRmv = 100000; //placeholder
			long maxTRmv = 0;      //placeholder
			for(int i = 0; i < RmvTreeTime.size(); i++){
				totalTreeRemove += RmvTreeTime.get(i);
				if(RmvTreeTime.get(i) < minTRmv){
					minTRmv = RmvTreeTime.get(i);
				}
				if(RmvTreeTime.get(i) > maxTRmv){
					maxTRmv = RmvTreeTime.get(i);
				}
			}
			float tRmvMean = totalTreeRemove / (float)numIter;
			float tRmvStdDev = (float) Math.pow(calcVari(
					RmvTreeTime, tRmvMean), 0.5);

			//HASH REMOVE OUTPUT
			long totalHashRemove = 0;
			long minHRmv = 100000; //placeholder
			long maxHRmv = 0;      //placeholder
			for(int i = 0; i < RmvHashTime.size() ; i++){
				totalHashRemove += RmvHashTime.get(i);
				if(RmvHashTime.get(i) < minHRmv){
					minHRmv = RmvHashTime.get(i);
				}
				if(RmvHashTime.get(i) > maxHRmv){
					maxHRmv = RmvHashTime.get(i);
				}
			}
			float hRmvMean = totalHashRemove / (float)numIter;
			float hRmvStdDev = (float) Math.pow(calcVari(
					RmvHashTime, hRmvMean), 0.5);

			//final output
			try{
				System.out.println("Results from " + numIter + " runs of " +
						"../" + fileName.getParentFile().getName() + "/" +
						fileName.getName());
			} catch (NullPointerException e){
				System.out.println("Results from " + numIter + " runs of " +
						"../" + fileName);
			}

			System.out.println();

			outputResults("HashMap: get", 
					minHGet, maxHGet, hPopMean, hGetStdDev);
			outputResults("HashMap: put",
					minHPop, maxHPop, hPopMean, hPopStdDev);
			outputResults("HashMap: floorKey",
					minHFlr, maxHFlr, hFlrMean, hFlrStdDev);
			outputResults("HashMap: remove", 
					minHRmv, maxHRmv, hRmvMean, hRmvStdDev);
			outputResults("TreeMap: get", 
					minTGet, maxTGet, tGetMean, tGetStdDev);
			outputResults("TreeMap: put",
					minTPop, maxTPop, tPopMean, tPopStdDev);
			outputResults("TreeMap: floorKey", 
					minTFlr, maxTFlr, tFlrMean, tFlrStdDev);
			outputResults("TreeMap: remove",
					minTRmv, maxTRmv, tRmvMean, tRmvStdDev);
		}
		else{
			System.out.println("Usage: MapBenchmark fileName.txt " +
					"numIterations");
			System.exit(0);
		}
	}

	/**
	 * Prints out the arguments in the format specified in the sample output
	 * file.
	 * 
	 * @param str the header before the dashes
	 * @param min min of header
	 * @param max max of header
	 * @param mean mean of header
	 * @param std the standard deviation of header
	 */
	private static void outputResults(String str, long min,
			long max, float mean, float std){

		System.out.println(str);
		System.out.println("--------------------");
		System.out.println("Min : " + min);
		System.out.println("Max : " + max);
		System.out.print("Mean : " + String.format("%.3f", mean) + "\n"); 
		System.out.print("Std Dev : " + String.format("%.3f", std) + "\n"); 

		System.out.println();

	}

	/**
	 * A helper method that will calculate the Variance for
	 * the given arguments. This method will assist in calculating the
	 * standard deviation of method calls. 
	 * 
	 * @param al the ArrayList whose elements will be added up
	 * @param mean the mean of the values in the ArrayList
	 */
	private static float calcVari(ArrayList<Long> al, float mean){
		float variance = 0;
		for(int i = 0; i < al.size(); i++){
			variance += Math.pow((mean - al.get(i)), 2);
		}
		return variance / al.size();
	}
	/**
	 * 
	 */

}
